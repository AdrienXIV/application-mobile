import React, { useState, useEffect } from 'react';
import { StyleSheet, FlatList } from 'react-native';
import { getCategories } from '../utils/API';
import { HandleError } from '../utils/HandleMessage';
import CardCategory from '../components/Cards/CardCategory';
import { Container } from '../components/Styles/Theme';

/**
 * Page d'accueil affichant toutes les catégories des produits
 */
const Home = ({ navigation }) => {
  const [state, setState] = useState([]);

  useEffect(() => {
    getCategories()
      .then(({ data, status }) => {
        if (status === 200) setState(data);
      })
      .catch(e => HandleError('Error lors de la récupération du catalogue Gusto Coffee', e.response.data.error, e.response.status));
  }, []);

  return (
    <Container alignItems='center'>
      <FlatList
        style={{ width: '100%' }}
        contentContainerStyle={styles.container}
        data={state}
        numColumns={2}
        renderItem={({ item }) => <CardCategory navigation={navigation} category={item} />}
        keyExtractor={item => item._id}
      />
    </Container>
  );
};

const styles = StyleSheet.create({
  container: {
    alignSelf: 'center',
    paddingBottom: '5%',
  },
});

export default Home;
