import React, { useState, useEffect } from 'react';
import { View, ScrollView, StyleSheet, Text, TouchableOpacity, Modal, TouchableHighlight, ActivityIndicator } from 'react-native';
import { useCookies } from 'react-cookie';
import SyncStorage from 'sync-storage';
import { TextInput } from 'react-native-gesture-handler';
import axios from 'axios';
import { socket as io } from '../utils/SocketIO';
import { getBasket, confirmPayment } from '../utils/API';
import { HandleError } from '../utils/HandleMessage';
import SummaryOrder from '../components/SummaryOrder';
import WaitingItem from '../components/WaitingItem';

/**
 * Page qui affiche l'étape du paiement pour l'envoyer au serveur (et au Backoffice)
 */
const Payment = ({ navigation, route }) => {
  const [waitingPayment, setWaitingPayment] = useState(false);
  const [disabled, setDisabled] = useState(true);
  const [socket, setSocket] = useState(null);
  const [articles, setArticles] = useState([]);
  const [reservations, setReservations] = useState([]);
  const [totalPrice, setTotalPrice] = useState(0);
  const [points, setPoints] = useState(0);
  const [modalVisible, setModalVisible] = useState(false);
  const [message, setMessage] = useState('');
  const [dataLoaded, setDataLoaded] = useState(false);
  const [, , removeCookie] = useCookies(['user', 'shoppingBasket']);
  const [state, setState] = useState({
    number: '',
    exp: '',
    cvc: '',
  });

  useEffect(() => {
    setSocket(io);
    getBasket(route.params.basket)
      .then(({ status, data }) => {
        if (status === 200) {
          setReservations(data.basket.reservations);
          setArticles(data.basket.articles);
          setTotalPrice(data.basket.total_price);
          setPoints(data.basket.preview_fidelity_points);
          setDataLoaded(true);
        }
      })
      .catch(e => HandleError('Error lors de la récupération de la commande', e.response.data.error, e.response.status));
  }, [route.params.basket, SyncStorage.get('confirmBasket') === 2]);

  useEffect(() => {
    // tests carte de crédit
    let error = 0;
    if (!/^[0-9]{16}$/.test(state.number)) {
      error++;
    }
    if (!/^(0[1-9]|1[0-2])\/[0-9]{2}$/.test(state.exp)) {
      error++;
    }
    if (!/^[0-9]{3}$/.test(state.cvc)) {
      error++;
    }
    error === 0 ? setDisabled(false) : setDisabled(true);
  }, [state]);

  const stripeRequest = async () => {
    setWaitingPayment(true);
    const expSplited = state.exp.split('/');
    const expMonth = expSplited[0];
    // si on entre 21 alors on met 2021
    const expYear = expSplited[1].length === 2 ? `20${expSplited[1]}` : expSplited[1];
    const stripeData = {
      number: state.number,
      exp_month: Number(expMonth),
      exp_year: Number(expYear),
      cvc: state.cvc,
    };
    try {
      const { data } = await axios.post('/order/mobile/stripe-payment', { card: stripeData });

      const payment = await confirmPayment({
        _id: route.params.basket,
        currency: 'euro',
        payment_method: 'bank card',
        id_stripe: data.id,
      });
      // s'il n'y a que des réservations, pas besoin d'envoyer au back office par websocket
      if (articles.length !== 0) socket.emit('new-order', { _id: payment.data._id });
      // suppression des infos relatives au panier
      removeCookie('shoppingBasket');
      removeCookie('reservation');
      SyncStorage.remove('basket');
      SyncStorage.set('confirmBasket', 0);
      // mise à jour du message à afficher dans une modal
      setMessage(payment.data.message);
      setModalVisible(true);
    } catch (e) {
      HandleError('Erreur lors du paiement', e.response.data.error, e.response.status);
    }
    setWaitingPayment(false);
  };

  if (!dataLoaded) return <WaitingItem message='Récupération de la commande...' logoName='shipping-fast' />;
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Text style={{ fontSize: 20, fontWeight: 'bold', textAlign: 'center' }}>Récapitulatif de la commande</Text>

      <SummaryOrder articles={articles} reservations={reservations} price={totalPrice} points={points} />

      <View style={{ marginTop: 10, marginBottom: 10 }}>
        <TextInput
          onChangeText={value => setState(prev => ({ ...prev, number: value }))}
          textContentType='text'
          maxLength={16}
          value={state.number}
          style={styles.textInput}
          placeholder='Numéro de la carte'
        />
      </View>
      <View style={{ marginTop: 10, marginBottom: 10, display: 'flex', flexDirection: 'row' }}>
        <TextInput
          onChangeText={value => setState(prev => ({ ...prev, exp: value }))}
          textContentType='text'
          maxLength={5}
          value={state.expmonth}
          style={styles.textInput}
          placeholder='MM/AA'
        />
        <TextInput
          onChangeText={value => setState(prev => ({ ...prev, cvc: value }))}
          textContentType='text'
          maxLength={3}
          value={state.cvc}
          style={styles.textInput}
          placeholder='CVC'
        />
      </View>
      <View style={{ marginTop: 10, marginBottom: 10 }} />

      <View style={{ flex: 1, justifyContent: 'flex-end' }}>
        {/* On met un indicateur lors de l'attente du paiement */}
        {waitingPayment ? (
          <ActivityIndicator size='large' color='#c3a684' />
        ) : (
          <>
            <TouchableOpacity disabled={disabled} style={{ ...styles.buttonPayment, opacity: disabled ? 0.5 : 1 }} onPress={stripeRequest}>
              <Text style={{ color: 'white', fontSize: 20 }}>Valider le paiement</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.buttonContinuePurchases} onPress={() => continuePurchases()}>
              <Text style={{ fontSize: 17 }}>Continuer mes achats</Text>
            </TouchableOpacity>
          </>
        )}
      </View>

      <Modal
        animationType='slide'
        presentationStyle='pageSheet'
        hardwareAccelerated
        statusBarTranslucent
        transparent={false}
        visible={modalVisible}
        onRequestClose={() => closeModal()}
      >
        <View style={styles.modalContainer}>
          <View>
            <Text style={{ fontSize: 20 }}>{message}</Text>

            <TouchableHighlight style={styles.buttonHideModal} onPress={() => closeModal()}>
              <Text style={{ color: 'white', fontSize: 17 }}>Revenir à l&apos;écran d&apos;accueil</Text>
            </TouchableHighlight>
          </View>
        </View>
      </Modal>
    </ScrollView>
  );

  // ##############################
  //           FONCTIONS
  // ##############################

  function continuePurchases() {
    SyncStorage.set('confirmBasket', 1);
    navigation.reset({
      index: 0,
      routes: [{ name: 'Accueil' }],
    });
  }

  function closeModal() {
    setModalVisible(!modalVisible);
    navigation.reset({
      index: 0,
      routes: [{ name: 'Accueil' }],
    });
  }
};
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    padding: 15,
  },
  text: {
    marginBottom: 10,
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -1, height: 1 },
    textShadowRadius: 10,
  },
  textInput: {
    flex: 1,
    backgroundColor: 'white',
    borderColor: 'silver',
    borderWidth: 1,
    padding: 5,
    borderRadius: 5,
    textAlign: 'center',
  },
  buttonSignin: {
    marginTop: 25,
    height: 50,
    backgroundColor: '#4cd137',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonPayment: {
    marginTop: 25,
    width: '100%',
    padding: 10,
    borderRadius: 25,
    backgroundColor: '#4cd137',
    alignItems: 'center',
    alignSelf: 'center',
  },
  buttonContinuePurchases: {
    marginTop: '5%',
    width: '100%',
    padding: 10,
    borderRadius: 25,
    backgroundColor: 'white',
    borderColor: 'silver',
    borderWidth: 1,
    alignItems: 'center',
    alignSelf: 'center',
  },
  modalContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonHideModal: {
    marginTop: 25,
    padding: 15,
    borderRadius: 5,
    backgroundColor: '#0652DD',
    alignItems: 'center',
    alignSelf: 'center',
  },
});
export default Payment;
