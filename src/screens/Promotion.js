import React, { useState, useEffect } from 'react';
import { View, StyleSheet, FlatList, Text } from 'react-native';
import { useCookies } from 'react-cookie';
import { getPromotions } from '../utils/API';
import { HandleError, NotifyMessage } from '../utils/HandleMessage';
import CardPromo from '../components/Cards/CardPromo';

const today = new Date();
const tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

/**
 * Page qui affiche toutes les infos du produit (nom, photo, options) ainsi que la possibilité de l'ajouter au panier
 * avec le choix de la quantité
 */
const Promotion = ({ route }) => {
  const [state, setState] = useState([]);
  const [cookie, setCookie] = useCookies(['user', 'shoppingBasket']);

  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    getPromotions()
      .then(({ data, status }) => {
        if (status === 200) {
          setState(data.sort((a, b) => a.offer - b.offer));
          setDataLoaded(true);
        }
      })
      .catch(e => HandleError('Error lors de la récupération du produit', e.response.data.error, e.response.status));
  }, [route, cookie.user]);

  if (!dataLoaded) return <View />;
  return (
    <View style={styles.container}>
      {showFidelityPoints()}

      <FlatList
        data={state}
        renderItem={({ item }) => <CardPromo addProductIntoShoppingBasket={() => addProductIntoShoppingBasket(item)} product={item} />}
        keyExtractor={product => product._id}
        numColumns={2}
      />
    </View>
  );

  // ##############################
  //           FONCTIONS
  // ##############################

  function showFidelityPoints() {
    return cookie.user.fidelity_points > 0 ? (
      <View style={styles.pointContainer}>
        <Text style={styles.pointText}>{`${cookie.user.fidelity_points} points`}</Text>
      </View>
    ) : (
      <View style={styles.pointContainer}>
        <Text style={styles.pointText}>Vous n&apos;avez aucun point de fidélité</Text>
      </View>
    );
  }

  function addProductIntoShoppingBasket(product) {
    const basket = cookie.shoppingBasket || [];

    basket.push({
      _id: product._id,
      name: product.name,
      categories: product.category_id,
      productPrice: product.price,
      totalPrice: 0,
      quantity: 1,
      points: -product.offer,
      productPoints: product.points,
      options: [],
      totalPriceIOptions: 0,
      priceOptions: 0,
      promo: true,
    });

    // mise à jour du cookie avec les nouveaux points de fidélité
    const updateUserCookie = cookie.user;
    updateUserCookie.fidelity_points -= product.offer;
    setCookie('user', updateUserCookie, {
      expires: tomorrow,
    });
    //
    setCookie('shoppingBasket', basket, {
      expires: tomorrow,
    });
    NotifyMessage(`${product.name} ajouté`);
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  pointContainer: {
    width: '100%',
    padding: 15,
    backgroundColor: '#b33939',
  },
  pointText: {
    fontSize: 30,
    fontWeight: 'bold',
    textAlign: 'center',
    color: 'white',
  },
});
export default Promotion;
