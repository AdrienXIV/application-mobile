import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, TouchableOpacity, Picker } from 'react-native';
import { useCookies } from 'react-cookie';
import { getDayReservation } from '../utils/API';
import { HandleError, NotifyMessage } from '../utils/HandleMessage';
import { socket as io } from '../utils/SocketIO';
import { useAuth } from '../hooks/useAuth';
import { Container } from '../components/Styles/Theme';
import WaitingItem from '../components/WaitingItem';
import { dateConverterWithDayWithoutHours } from '../utils/DateConverter';

const hoursValue = [
  {
    label: '8h',
    value: 8,
  },
  {
    label: '9h',
    value: 9,
  },
  {
    label: '10h',
    value: 10,
  },
  {
    label: '11h',
    value: 11,
  },
  {
    label: '12h',
    value: 12,
  },
  {
    label: '13h',
    value: 13,
  },
  {
    label: '14h',
    value: 14,
  },
  {
    label: '15h',
    value: 15,
  },
  {
    label: '16h',
    value: 16,
  },
  {
    label: '17h',
    value: 17,
  },
  {
    label: '18h',
    value: 18,
  },
];

const today = new Date();
const tomorrow = new Date(today.getTime() + 24 * 60 * 60 * 1000);

const Reservation = ({ route, navigation }) => {
  const { user } = useAuth();

  const [socket, setSocket] = useState(null);
  const [state, setState] = useState({});

  const [startValues, setStartValues] = useState([]);
  const [endValues, setEndValues] = useState([]);

  const [disabled, setDisabled] = useState(false);
  const [message, setMessage] = useState('');

  const [start, setStart] = useState(8);
  const [end, setEnd] = useState(8);
  const [price, setPrice] = useState(0);

  const [dataLoaded, setDataLoaded] = useState(false);
  const [cookie, setCookie] = useCookies(['shoppingBasket', 'reservation', 'user']);

  const year = new Date(route.params.year, route.params.month, route.params.day).getFullYear();
  const month = new Date(route.params.year, route.params.month, route.params.day).getMonth();
  const day = new Date(route.params.year, route.params.month, route.params.day).getDate();

  useEffect(() => {
    (async () => {
      setSocket(io);
      const dayToday = new Date().getDate();
      const monthToday = new Date().getMonth() + 1;
      const yearToday = new Date().getFullYear();
      const offset = 8;
      const temporaryStartArray = new Array(10).fill({ label: '', value: 0 });
      const temporaryEndArray = new Array(10).fill({ label: '', value: 0 });
      for (let i = 0; i <= 10; i++) {
        if (year >= yearToday && month >= monthToday && day >= dayToday) {
          if (i + offset >= new Date().getHours()) {
            temporaryStartArray[i] = { label: `${i + offset}h`, value: i + offset };
            // si ce sont els jours d'après le jour actuel ou les mois d'après
          } else if (day > dayToday || month > monthToday) {
            temporaryStartArray[i] = { label: `${i + offset}h`, value: i + offset };
          } else temporaryStartArray[i] = { label: `${i + offset}h - Réservation impossible`, value: 'reserve' };

          if (i + offset > new Date().getHours()) {
            temporaryEndArray[i] = { label: `${i + offset}h`, value: i + offset };
          } else if (day > dayToday || month > monthToday) {
            temporaryEndArray[i] = { label: `${i + offset}h`, value: i + offset };
          } else temporaryEndArray[i] = { label: `${i + offset}h - Réservation impossible`, value: 'reserve' };
        } else {
          temporaryStartArray[i] = { label: `${i + offset}h - Réservation impossible`, value: 'reserve' };
          temporaryEndArray[i] = { label: `${i + offset}h - Réservation impossible`, value: 'reserve' };
        }
      }

      try {
        const { data } = await getDayReservation(route.params._id, route.params.day, route.params.month, route.params.year);
        setState(route.params.state);

        // On parcourt notre tableau de réservation
        data.forEach(reservation => {
          // On parcourt le tableau de modèle d'heures
          hoursValue.forEach((hour, i) => {
            // Si l'heure du modele est supérieure à l'heure courante
            // if (hour.value > new Date().getHours()) {
            // // Si l'heure du modele est plus petite que l'heure l'heure fin de la reservation
            if ((hour.value >= reservation.start && hour.value < reservation.end) || temporaryStartArray[i].value === 'reserve') {
              // On affiche que l'emplacement est deja reservé a cette heure
              if (temporaryStartArray[i].value !== 'reserve')
                temporaryStartArray[i] = { label: `${hour.value}h - Emplacement déja réservé`, value: 'reserve' };
            } else {
              // Sinon on affiche l'heure
              temporaryStartArray[i] = { label: `${hour.value}h`, value: hour.value };
            }

            // Si l'heure du modele est plus grande ou égale a l'heure de début de réservation
            // et plus petite ou égale a l'heure de fin de réservation
            if ((hour.value > reservation.start && hour.value <= reservation.end) || temporaryEndArray[i].value === 'reserve') {
              // On affiche que l'emplacement est deja reservé a cette heure
              if (temporaryEndArray[i].value !== 'reserve')
                temporaryEndArray[i] = { label: `${hour.value}h - Emplacement déja réservé`, value: 'reserve' };
            } else {
              // Sinon on affiche l'heure
              temporaryEndArray[i] = { label: `${hour.value}h`, value: hour.value };
            }
          });
        });
        setStartValues(temporaryStartArray);
        setEndValues(temporaryEndArray);

        setDataLoaded(true);
      } catch (e) {
        HandleError('Erreur lors de la récupération des réservations', e.response.data.error, e.response.status);
      }
    })();
  }, [route.params, socket]);

  useEffect(() => {
    if (start > end) {
      setMessage('Horaires invalides');
      setDisabled(true);
    } else if (start === 'reserve' && end === 'reserve') {
      setMessage('Horaires réservées');
      setDisabled(true);
    } else if (start === 'reserve') {
      setMessage('Horaire de début réservée');
      setDisabled(true);
    } else if (end === 'reserve') {
      setMessage('Horaire de fin réservée');
      setDisabled(true);
    } else if (start === end) {
      setMessage('Horaires invalides');
      setDisabled(true);
    } else {
      setMessage('');
      setDisabled(false);
    }

    if (dataLoaded) {
      if (start >= 9 && end <= 16 && end - start >= 3) {
        setPrice(state.location_category_id.price * (end - start) - state.location_category_id.price);
      } else {
        setPrice(state.location_category_id.price * (end - start));
      }
    }
  }, [start, end, socket]);

  // sécurité pour éviter de réserver quand le jour est dépassé
  if (day < new Date().getDate() || month < new Date().getMonth() || year < new Date().getFullYear())
    return (
      <Container alignItems='center' justifyContent='center'>
        <Text style={{ color: 'white', fontSize: 20 }}>Jour invalide</Text>
      </Container>
    );
  if (!dataLoaded) return <WaitingItem message='Récupération des réservations...' logoName='laptop-house' />;
  return (
    <Container alignItems='center' bg='white'>
      <View style={{ padding: 50, alignItems: 'center', backgroundColor: '#b33939', width: '100%' }}>
        <Text style={{ fontSize: 17, color: 'white' }}>
          {dateConverterWithDayWithoutHours(new Date(route.params.year, route.params.month - 1, route.params.day))}
        </Text>
        <Text style={{ fontSize: 17, color: 'white' }}>La 3ème heure offerte de 9h à 16h</Text>
      </View>

      <View style={{ width: '100%', padding: 20, flexDirection: 'row', justifyContent: 'space-between' }}>
        <View>
          <Text style={{ textAlign: 'center' }}>Début</Text>
          <Picker
            selectedValue={start}
            mode='dropdown'
            style={{ height: 50, width: 150 }}
            onValueChange={itemValue => {
              setStart(itemValue);
            }}
          >
            {startValues.map((item, index) => (
              <Picker.Item value={item.value} label={item.label} key={index.toString()} />
            ))}
          </Picker>
        </View>
        <View>
          <Text style={{ textAlign: 'center' }}>Fin</Text>
          <Picker selectedValue={end} mode='dropdown' style={{ height: 50, width: 150 }} onValueChange={itemValue => setEnd(itemValue)}>
            {endValues.map((item, index) => index !== 0 && <Picker.Item value={item.value} label={item.label} key={index.toString()} />)}
          </Picker>
        </View>
      </View>
      <View style={{ alignItems: 'center', justifyContent: 'center', flex: 1, width: '100%' }}>
        <View
          style={{
            flex: 1,
            width: '100%',
            alignItems: 'center',
            justifyContent: 'center',
          }}
        >
          <Text style={{ fontSize: 17 }}>{showHoursText()}</Text>
          <Text style={{ fontSize: 17 }}>{showPriceText()}</Text>
          <Text style={{ fontSize: 17 }}>{showPointsText()}</Text>
        </View>
        <View
          style={{
            margin: '7.5%',
            width: '100%',
            alignItems: 'center',
            justifyContent: 'flex-end',
          }}
        >
          <Text style={{ fontSize: 17, color: 'red', textAlign: 'center', margin: 15 }}>{message}</Text>
          <TouchableOpacity
            disabled={disabled}
            style={[styles.buttonAddReservation, { opacity: disabled ? 0.5 : 1 }]}
            onPress={() => addReservation()}
          >
            <Text style={{ color: 'white', fontSize: 17, fontWeight: 'bold' }}>Réserver</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Container>
  );

  // ##############################
  //           FONCTIONS
  // ##############################

  function showHoursText() {
    return disabled ? '' : `Horaires de réservation : ${start}h - ${end}h`;
  }
  function showPriceText() {
    const priceWithoutPromo = (end - start) * state.location_category_id.price;
    const priceWithPromo = price;
    return disabled
      ? ''
      : end - start >= 3 && start >= 9 && end <= 16
      ? `Tarif : ${priceWithoutPromo}€ - ${priceWithoutPromo - priceWithPromo}€ = ${priceWithPromo}€ (${end - start}h)`
      : `Tarif : ${price}€ (${end - start}h)`;
  }
  function showPointsText() {
    return disabled ? '' : `Points de fidélité : ${(end - start) * state.location_category_id.points} points`;
  }
  function addReservation() {
    const hours = end - start;
    const points = hours * state.location_category_id.points;

    const reservation = cookie.reservation || [];
    socket.emit(
      'new-hour-reservation',
      {
        user_id: user.id,
        location_id: state._id,
        year: route.params.year,
        month: route.params.month,
        day: route.params.day,
        start,
        end,
        price,
        fidelity_points: points,
      },
      ({ data, code, error }) => {
        if (code === 201) {
          reservation.push({
            _id: data._id,
            price: state.location_category_id.price,
            totalPrice: price,
            date: route.params,
            points,
            category: state.location_category_id.name,
            name: state.name,
            hours,
            reservedHours: { start, end },
            createdAt: Date.now(),
          });

          setCookie('reservation', reservation, { expires: tomorrow });
          NotifyMessage(`${state.name} réservé`);
          navigation.goBack();
        } else {
          HandleError("Erreur lors de la réservation d'un espace", error, code);
        }
      },
    );
  }
};

const styles = StyleSheet.create({
  buttonAdd: {
    alignSelf: 'center',
    backgroundColor: '#c3a684',
    width: '66.66%',
    padding: 10,
    marginTop: 20,
    borderRadius: 25,
  },
  buttonAddText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
  buttonAddReservation: {
    width: '75%',
    height: 50,
    backgroundColor: '#4cd137',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Reservation;
