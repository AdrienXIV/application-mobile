import React, { useState, useEffect } from 'react';
import { View, StyleSheet, TextInput, Text, TouchableOpacity, ScrollView } from 'react-native';
import { Avatar } from 'react-native-elements';
import { Formik } from 'formik';
import { useAuth } from '../../hooks/useAuth';
import { getProfile } from '../../utils/API';
import { HandleError } from '../../utils/HandleMessage';

const EditProfile = ({ navigation }) => {
  const [state, setState] = useState({});
  const [dataLoaded, setDataLoaded] = useState(false);
  const [initialLetters, setInitialLetters] = useState(['A', 'A']);
  const { editMyProfile } = useAuth();

  useEffect(() => {
    getProfile()
      .then(({ status, data }) => {
        if (status === 200) {
          setState(data);
          setDataLoaded(true);
          // récupérer les initales du prénom et du nom
          const firstname = data.firstname.split('');
          const lastname = data.lastname.split('');
          setInitialLetters([firstname[0].toUpperCase(), lastname[0].toUpperCase()]);
        }
      })
      .catch(e => {
        HandleError('Error lors de la modification du profil', e.response.data.error, e.response.status);
      });
  }, []);
  if (!dataLoaded) return <View />;
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View
        style={{
          backgroundColor: '#b33939',
          height: 100,
          alignItems: 'center',
          marginBottom: '20%',
        }}
      >
        <Avatar
          size='xlarge'
          rounded
          title={`${initialLetters[0]}${initialLetters[1]}`}
          containerStyle={{ position: 'absolute', top: 25 }}
          overlayContainerStyle={{
            backgroundColor: '#b33939',
            borderColor: 'white',
            borderWidth: 5,
          }}
        />
      </View>
      <Formik
        initialValues={{ email: state.email, firstname: state.firstname, lastname: state.lastname }}
        onSubmit={values => {
          editMyProfile({ email: values.email, lastname: values.lastname, firstname: values.firstname }, navigation);
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View style={{ padding: 25 }}>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              <View style={[styles.inputContainer, { width: '47.5%' }]}>
                <Text style={styles.text}>Prénom</Text>
                <TextInput
                  onChangeText={handleChange('firstname')}
                  onBlur={handleBlur('firstname')}
                  textContentType='name'
                  value={values.firstname}
                  style={styles.textInput}
                  placeholder='Votre prénom'
                />
              </View>
              <View style={[styles.inputContainer, { width: '47.5%' }]}>
                <Text style={styles.text}>Nom</Text>
                <TextInput
                  onChangeText={handleChange('lastname')}
                  onBlur={handleBlur('lastname')}
                  textContentType='familyName'
                  value={values.lastname}
                  style={styles.textInput}
                  placeholder='Votre nom'
                />
              </View>
            </View>

            <View style={styles.inputContainer}>
              <Text style={styles.text}>Courriel</Text>
              <TextInput
                onChangeText={handleChange('email')}
                onBlur={handleBlur('email')}
                textContentType='emailAddress'
                value={values.email}
                style={styles.textInput}
                placeholder='courriel@gmail.com'
              />
            </View>

            <Text style={styles.editPassword} onPress={() => navigation.navigate('Modifier mon mot de passe')}>
              Modifier mon mot de passe
            </Text>

            <TouchableOpacity onPress={handleSubmit} style={styles.button}>
              <Text style={styles.buttonText}>Modifier mon profil</Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: '#ffffff',
  },
  inputContainer: { marginTop: 10, marginBottom: 10 },
  image: {
    width: 125,
    height: 125,
    marginBottom: 20,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  text: {
    marginBottom: 10,
    fontSize: 20,
  },
  textInput: {
    borderColor: 'silver',
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
  },
  button: {
    marginTop: '20%',
    height: 50,
    backgroundColor: '#b33939',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: 'white',
    fontWeight: 'bold',
  },
  editPassword: {
    fontSize: 15,
    marginTop: 10,
    textDecorationLine: 'underline',
  },
});
export default EditProfile;
