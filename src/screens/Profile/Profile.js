import React, { useEffect, useState } from 'react';
import { View, ScrollView, StyleSheet, Text, TouchableOpacity, Alert } from 'react-native';
import { ListItem, PricingCard } from 'react-native-elements';
import { faShoppingBasket, faUserAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { useCookies } from 'react-cookie';
import { getProfile } from '../../utils/API';
import { dateConverterWithDayWithHours } from '../../utils/DateConverter';
import { useAuth } from '../../hooks/useAuth';

const Profile = ({ navigation }) => {
  const { user } = useAuth();
  const [cookie] = useCookies(['user']);
  const { removeMyAccount, signout } = useAuth();
  const [state, setState] = useState({});
  const [dataLoaded, setDataLoaded] = useState(false);
  useEffect(() => {
    // stocker les infos du cookie dans une variable pour éviter une erreur lors de la deconnexion (suppression du cookie)
    getProfile().then(({ status, data }) => {
      if (status === 200) {
        setState(data);
        setDataLoaded(true);
      }
    });
  }, [navigation]);

  useEffect(() => {
    // mise à jour des points de fidélité sur le profil en fonction de l'ajout de le panier des produits/promo
    if (cookie.user) setState(prev => ({ ...prev, fidelity_points: cookie.user.fidelity_points }));
  }, [cookie.user, user]);

  if (!dataLoaded) return <View />;
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <PricingCard
        color='#4f9deb'
        title={`${state.firstname} ${state.lastname}`}
        price={state.fidelity_points === 0 ? '0 point' : `${state.fidelity_points} points`}
        info={[state.email, `Inscrit depuis le ${dateConverterWithDayWithHours(state.createdAt)}`]}
        button={{
          title: 'Convertir mes points',
          icon: 'cached',
          titleStyle: { marginLeft: 5 },
          onPress: () => navigation.navigate('Promotion', { points: state.fidelity_points }),
        }}
      />
      <View style={{ marginTop: 20 }}>
        {/* MODIFIER SON PROFIL */}
        <ListItem
          leftIcon={() => <FontAwesomeIcon icon={faUserAlt} size={25} />}
          chevron={{ size: 30 }}
          onPress={() => navigation.navigate('Modifier mon profil')}
          title='Modifier mon profil'
          pad={25}
          topDivider
        />

        {/* VOIR SES COMMANDES */}
        <ListItem
          leftIcon={() => <FontAwesomeIcon icon={faShoppingBasket} size={25} />}
          chevron={{ size: 30 }}
          onPress={() => navigation.navigate('Commandes')}
          title='Voir mes commandes'
          pad={25}
          bottomDivider
        />
      </View>

      {/* DECONNEXION */}
      <View style={styles.signoutContainer}>
        <Text onPress={signout} style={{ color: '#2980b9', fontSize: 17 }}>
          Se déconnecter
        </Text>
      </View>

      {/* SUPPRESSION COMPTE */}
      <TouchableOpacity
        style={styles.buttonRemoveAccount}
        onPress={() => {
          Alert.alert(`Suppression du compte ${state.firstname} ${state.lastname}`, 'Voulez-vous vraiment supprimer votre compte ?', [
            { text: 'NON', style: 'cancel' },
            { text: 'OUI', onPress: () => removeMyAccount() },
          ]);
        }}
      >
        <Text style={{ color: 'white', fontSize: 17, fontWeight: 'bold' }}>Supprimer mon compte</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
  },
  buttonShowOrders: {
    marginTop: 25,
    width: '100%',
    padding: 10,
    borderRadius: 5,
    backgroundColor: '#4cd137',
    alignItems: 'center',
    alignSelf: 'center',
  },
  signoutContainer: {
    marginTop: 20,
    alignItems: 'center',
    alignSelf: 'center',
  },
  buttonRemoveAccount: {
    marginTop: 20,
    width: '100%',
    padding: 10,
    backgroundColor: '#EA2027',
    alignItems: 'center',
    alignSelf: 'center',
  },
});
export default Profile;
