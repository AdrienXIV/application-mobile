import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Text, FlatList } from 'react-native';
import { getCoworkingCategoryPlaces } from '../../utils/API';
import { HandleError } from '../../utils/HandleMessage';
import CardCoworking from '../../components/Cards/CardCoworking';
import { Container } from '../../components/Styles/Theme';
import EmptyItem from '../../components/EmptyItem';

/**
 * Page qui affiche tous les produits correspondant à une catégorie
 */
const CategoryCoworking = ({ route, navigation }) => {
  const [state, setState] = useState([]);
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    getCoworkingCategoryPlaces(route.params.category._id)
      .then(({ status, data }) => {
        if (status === 200) {
          setState(data);
          setDataLoaded(true);
        }
      })
      .catch(e => HandleError('Error lors de la récupération des emplacements', e.response.data.error, e.response.status));
  }, [route.params.category._id]);

  if (!dataLoaded) return <View />;
  if (state.length === 0) return <EmptyItem message='Aucun espace de travail' logoName='briefcase' />;
  return (
    <Container alignItems='center'>
      <FlatList
        ListHeaderComponent={header}
        contentContainerStyle={{ paddingBottom: '5%' }}
        columnWrapperStyle={{ justifyContent: 'center' }}
        style={{ flex: 1 }}
        data={state}
        renderItem={place => <CardCoworking navigation={navigation} place={place.item} />}
        keyExtractor={place => place._id}
        numColumns={2}
      />
    </Container>
  );

  function header() {
    return (
      <>
        <Text style={styles.header}>{`${route.params.category.name} - ${route.params.category.price}€`}</Text>
        <Text style={styles.subheader}>{`${route.params.category.place_number} ${route.params.category.place_number > 1 ? 'places' : 'place'}`}</Text>
      </>
    );
  }
};

const styles = StyleSheet.create({
  header: {
    fontSize: 35,
    textAlign: 'center',
    margin: 25,
    marginBottom: 5,
    color: 'white',
    fontWeight: 'bold',
  },
  subheader: {
    fontSize: 25,
    textAlign: 'center',
    marginBottom: 25,
    color: 'white',
    fontWeight: 'bold',
  },
});
export default CategoryCoworking;
