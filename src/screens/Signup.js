import React, { useState } from 'react';
import { View, StyleSheet, TextInput, Text, Image, TouchableOpacity, ScrollView, Linking } from 'react-native';
import { CheckBox } from 'react-native-elements';
import { Formik } from 'formik';
import { useAuth } from '../hooks/useAuth';
import { url_site as __URL } from '../../info.json';

const Signup = () => {
  const { signup } = useAuth();
  const [isChecked, setIsChecked] = useState(false);
  const [message, setMessage] = useState([
    {
      email: 'Format incorrect',
      isValid: false,
    },
    {
      passwordLength: 'Le mot de passe doit contenir au moins 10 caractères',
      isValid: false,
    },
    {
      passwordMaj: 'Le mot de passe doit contenir une majuscule',
      isValid: false,
    },
    {
      passwordMin: 'Le mot de passe doit contenir une minuscule',
      isValid: false,
    },
    {
      passwordNum: 'Le mot de passe doit contenir un chiffre',
      isValid: false,
    },
  ]);
  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Image style={styles.image} source={require(`../../images/logo.png`)} />
      <Formik
        initialValues={{ email: '', password: '', firstname: '', lastname: '' }}
        onSubmit={values => {
          signup(values.lastname, values.firstname, values.email, values.password);
        }}
      >
        {({ handleChange, handleBlur, handleSubmit, values }) => (
          <View>
            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
              {/* NOM */}
              <View style={[styles.inputContainer, { width: '47.5%' }]}>
                <Text style={styles.text}>Prénom</Text>
                <TextInput
                  onChangeText={handleChange('firstname')}
                  onBlur={handleBlur('firstname')}
                  textContentType='name'
                  value={values.firstname}
                  style={[styles.textInput, { borderColor: values.firstname.length > 0 ? 'green' : 'red' }]}
                  placeholder='Votre prénom'
                />
              </View>

              {/* PRENOM */}
              <View style={[styles.inputContainer, { width: '47.5%' }]}>
                <Text style={styles.text}>Nom</Text>
                <TextInput
                  onChangeText={handleChange('lastname')}
                  onBlur={handleBlur('lastname')}
                  textContentType='familyName'
                  value={values.lastname}
                  style={[styles.textInput, { borderColor: values.lastname.length > 0 ? 'green' : 'red' }]}
                  placeholder='Votre nom'
                />
              </View>
            </View>

            {/* COURRIEL */}
            <View style={styles.inputContainer}>
              <Text style={styles.text}>Courriel</Text>
              <TextInput
                onChangeText={handleChange('email')}
                onChange={e => checkEmail(e.nativeEvent.text)}
                onBlur={handleBlur('email')}
                textContentType='emailAddress'
                value={values.email}
                style={[styles.textInput, { borderColor: message[0].isValid ? 'green' : 'red' }]}
                placeholder='courriel@gmail.com'
              />
              <Text style={{ color: 'red', fontStyle: 'italic', padding: 2 }}>{!message[0].isValid ? message[0].email : ''}</Text>
            </View>

            {/* MOT DE PASSE */}
            <View style={styles.inputContainer}>
              <Text style={styles.text}>Mot de passe</Text>
              <TextInput
                onChangeText={handleChange('password')}
                onChange={e => checkPassword(e.nativeEvent.text)}
                onBlur={handleBlur('password')}
                textContentType='password'
                value={values.password}
                secureTextEntry
                style={[
                  styles.textInput,
                  {
                    borderColor: message[1].isValid && message[2].isValid && message[3].isValid && message[4].isValid ? 'green' : 'red',
                  },
                ]}
              />
              <Text style={{ padding: 2, fontStyle: 'italic' }}>Le mot de passe doit contenir :</Text>
              <Text style={{ padding: 2, fontStyle: 'italic', color: message[1].isValid ? 'green' : 'red' }}>au moins 10 caractères</Text>
              <Text style={{ padding: 2, fontStyle: 'italic', color: message[2].isValid ? 'green' : 'red' }}>une majuscule</Text>
              <Text style={{ padding: 2, fontStyle: 'italic', color: message[3].isValid ? 'green' : 'red' }}>une minuscule</Text>
              <Text style={{ padding: 2, fontStyle: 'italic', color: message[4].isValid ? 'green' : 'red' }}>un chiffre.</Text>
            </View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
              <CheckBox containerStyle={{ paddingLeft: 0, marginLeft: 0 }} checked={isChecked} onPress={() => setIsChecked(!isChecked)} />
              <Text>J&apos;accepte les </Text>
              <TouchableOpacity onPress={() => Linking.openURL(`${__URL}/cgu-cgv`)}>
                <Text style={{ color: '#3498db' }}>conditions d&apos;utilisation</Text>
              </TouchableOpacity>
            </View>

            <View style={{ marginTop: '5%', opacity: isChecked ? 0 : 1 }}>
              <Text style={{ textAlign: 'center', color: 'red' }}>Veuillez accepter les conditions pour valdier votre inscription !</Text>
            </View>
            <TouchableOpacity
              disabled={!checkFormulary(values.firstname, values.lastname)}
              onPress={handleSubmit}
              style={[styles.button, { opacity: checkFormulary(values.firstname, values.lastname) ? 1 : 0.5 }]}
            >
              <Text style={styles.buttonText}>S&apos;inscrire</Text>
            </TouchableOpacity>
          </View>
        )}
      </Formik>
    </ScrollView>
  );

  /**
   * Vérification de la syntaxe du courriel
   * @param {String} email email
   */
  function checkEmail(email) {
    const newMessage = message;
    if (/\S+@\S+\.\S+/.test(email)) newMessage[0].isValid = true;
    else newMessage[0].isValid = false;
    setMessage(newMessage);
  }

  /**
   * Vérification de la syntaxe du mot de passe
   * @param {String} password mot de passe
   */
  function checkPassword(password) {
    const newMessage = message;
    // taille du mot de passe
    if (password.length >= 10) newMessage[1].isValid = true;
    else newMessage[1].isValid = false;
    // au moins un caractère en majuscule
    if (/^(?=.*[A-Z])/.test(password)) newMessage[2].isValid = true;
    else newMessage[2].isValid = false;
    // au moins un caractère en minuscule
    if (/^(?=.*[a-z])/.test(password)) newMessage[3].isValid = true;
    else newMessage[3].isValid = false;
    // au moins un chiffre
    if (/^(?=.*[0-9])/.test(password)) newMessage[4].isValid = true;
    else newMessage[4].isValid = false;

    setMessage(newMessage);
  }

  /**
   *  Vérification si le nom et le prénom ont bien été renseignés avec le mot de passe et courriel
   * @param {String} firstname
   * @param {String} lastname
   */
  function checkFormulary(firstname, lastname) {
    // si tous les champs sont valides
    return Boolean(message.find(val => !val.isValid) === undefined && isChecked && firstname.length > 0 && lastname.length > 0);
  }
};

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 15,
    justifyContent: 'center',
    backgroundColor: '#ffffff',
  },
  inputContainer: { marginTop: 10, marginBottom: 10 },
  image: {
    width: 125,
    height: 125,
    marginBottom: 20,
    alignSelf: 'center',
    resizeMode: 'contain',
  },
  text: {
    marginBottom: 10,
    fontSize: 20,
  },
  textInput: {
    borderColor: 'silver',
    borderWidth: 1,
    padding: 10,
    borderRadius: 5,
  },
  button: {
    marginTop: 25,
    height: 50,
    backgroundColor: '#3498db',
    borderRadius: 25,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    fontSize: 20,
    color: 'white',
  },
  signup: {
    fontSize: 15,
    marginTop: 25,
    textDecorationLine: 'underline',
    textAlign: 'center',
  },
});
export default Signup;
