import React, { useState, useEffect } from 'react';
import { View, ScrollView, StyleSheet, Text, Image, TouchableOpacity } from 'react-native';
import { CheckBox } from 'react-native-elements';
import InputSpinner from 'react-native-input-spinner';
import { getProduct } from '../utils/API';
import { HandleError } from '../utils/HandleMessage';
import { Container } from '../components/Styles/Theme';
import ShowImage from '../utils/ShowImage';
import WaitingItem from '../components/WaitingItem';
import useBasket from '../hooks/useBasket';

/**
 * Page qui affiche toutes les infos du produit (nom, photo, options) ainsi que la possibilité de l'ajouter au panier
 * avec le choix de la quantité
 */
const Product = ({ route, navigation }) => {
  const [state, setState] = useState([]);
  const [dataLoaded, setDataLoaded] = useState(false);
  const [quantity, setQuantity] = useState(1);
  const [checkbox, setCheckbox] = useState([]);
  const { addProductIntoBasket } = useBasket(navigation);

  useEffect(() => {
    getProduct(route.params._id)
      .then(({ data, status }) => {
        if (status === 200) {
          data.option_id.forEach(item => {
            setCheckbox(prev => [
              ...prev,
              {
                _id: item._id,
                isChecked: false,
                name: item.name,
                price: item.price,
              },
            ]);
          });
          setState(data);
          setDataLoaded(true);
        }
      })
      .catch(e => HandleError('Error lors de la récupération du produit', e.response.data.error, e.response.status));
  }, [route.params._id]);

  if (!dataLoaded) return <WaitingItem message='Aucun produit' logoName='coffee' />;
  return (
    <Container>
      <Image style={[styles.image, { opacity: 0.5, backgroundColor: 'black' }]} source={ShowImage(state.category_id[0].name)} />
      <View style={styles.descriptionContainer}>
        <Text style={styles.descriptionText}>{`“ ${state.description} ”`}</Text>
      </View>
      <Text style={styles.header}>{`${state.name} - ${state.price}€`}</Text>
      <Text style={styles.subheader}>{`+ ${state.points} points`}</Text>

      <ScrollView style={styles.scrollContainer} contentContainerStyle={{ flexGrow: 1, padding: '5%' }}>
        {state.option_id
          ? state.option_id.map((option, index) => (
              <View key={index.toString()} style={{ flexDirection: 'row', alignItems: 'center' }}>
                <CheckBox
                  checked={checkbox[index].isChecked}
                  onPress={() => {
                    onChangeCheck(index);
                  }}
                  checkedIcon='dot-circle-o'
                  uncheckedIcon='circle-o'
                />
                <Text style={{ fontSize: 17 }}>{`${option.name} ${option.price || 0}€`}</Text>
              </View>
            ))
          : null}

        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <InputSpinner
            style={{ alignSelf: 'center', margin: 15 }}
            min={1}
            step={1}
            rounded={false}
            showBorder
            value={quantity}
            onChange={num => {
              setQuantity(num);
            }}
          />
          <TouchableOpacity
            style={styles.buttonAdd}
            onPress={() => {
              addProductIntoBasket(state, quantity, checkbox);
              navigation.goBack();
            }}
          >
            <Text style={styles.buttonAddText}>Ajouter</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </Container>
  );

  // ##############################
  //           FONCTIONS
  // ##############################

  function onChangeCheck(index) {
    const newCheckbox = checkbox;
    newCheckbox[index].isChecked = !newCheckbox[index].isChecked;
    setCheckbox(newCheckbox);
  }
};

const styles = StyleSheet.create({
  scrollContainer: {
    flex: 1,
    backgroundColor: 'white',
    width: '100%',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  header: {
    fontSize: 30,
    marginTop: 15,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
  },
  subheader: {
    fontSize: 20,
    marginTop: 5,
    marginBottom: 15,
    fontWeight: 'bold',
    color: 'white',
    alignSelf: 'center',
  },
  descriptionContainer: {
    alignSelf: 'center',
    justifyContent: 'center',
    height: '33.33%',
    position: 'absolute',
    zIndex: 100,
    elevation: 100,
  },
  descriptionText: {
    fontSize: 20,
    fontStyle: 'italic',
    textAlign: 'center',
    color: 'white',
    textShadowColor: 'rgba(0, 0, 0, 0.75)',
    textShadowOffset: { width: -2, height: 2 },
    textShadowRadius: 10,
  },
  image: {
    width: '100%',
    height: '33.33%',
  },
  text: { margin: 5 },
  buttonAdd: {
    alignSelf: 'center',
    backgroundColor: '#c3a684',
    width: '66.66%',
    padding: 10,
    marginTop: 20,
    borderRadius: 25,
  },
  buttonAddText: {
    alignSelf: 'center',
    color: 'white',
    fontSize: 20,
    fontWeight: 'bold',
  },
});
export default Product;
