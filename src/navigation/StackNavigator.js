// * https://reactnavigation.org/docs/auth-flow/
import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { useAuth } from '../hooks/useAuth';
import TabNavigator from './TabNavigator';
import { UserStackScreen } from './StackScreen';

const StackNavigator = () => {
  const { user } = useAuth();

  // si l'utilisateur n'est pas connecté alors on affiche la page de connexion
  // sinon on affiche l'interface mobile de Gusto Coffee
  return <NavigationContainer>{!user ? <UserStackScreen /> : <TabNavigator />}</NavigationContainer>;
};

export default StackNavigator;
