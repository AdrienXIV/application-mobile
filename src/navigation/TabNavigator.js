import React from 'react';
import { View, Text } from 'react-native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome';
import { faHome, faUser, faKey, faShoppingBasket, faLaptopHouse, faCoffee } from '@fortawesome/free-solid-svg-icons';
import { useCookies } from 'react-cookie';
import { HomeStackScreen, CoworkingStackScreen, ProfileStackScreen, BasketStackScreen } from './StackScreen';

const Tab = createBottomTabNavigator();

// affichage de l'icon panier + compteur
function IconWithBadge({ icon, badgeCount, color, size }) {
  return (
    <View style={{ width: 24, height: 24, margin: 5 }}>
      <FontAwesomeIcon icon={icon} size={size} color={color} />

      {badgeCount > 0 && (
        <View
          style={{
            // On React Native < 0.57 overflow outside of parent will not work on Android, see https://git.io/fhLJ8
            position: 'absolute',
            right: -28,
            top: -7,
            padding: 1,
            backgroundColor: 'red',
            borderRadius: 100,
            width: 27,
            height: 25,

            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Text style={{ color: 'white', fontSize: 11, fontWeight: 'bold' }}>{badgeCount}</Text>
        </View>
      )}
    </View>
  );
}

// vérifie si on clique sur le panier ou non pour afficher un compteur
function CheckIconName({ icon, size, color, name }) {
  const [cookie] = useCookies(['shoppingBasket', 'reservation']);
  let count = 0;
  if (cookie.shoppingBasket) count += cookie.shoppingBasket.length;
  if (cookie.reservation) count += cookie.reservation.length;

  return name === 'Panier' ? (
    <IconWithBadge icon={icon} size={size} color={color} badgeCount={count} />
  ) : (
    <FontAwesomeIcon icon={icon} size={size} color={color} />
  );
}

const TabNavigator = () => (
  <Tab.Navigator
    screenOptions={({ route }) => ({
      tabBarIcon: ({ color }) => {
        let iconName;
        switch (route.name) {
          case 'Accueil':
            iconName = faCoffee;
            break;
          case 'Espaces de travail':
            iconName = faLaptopHouse;
            break;
          case 'Profil':
            iconName = faUser;
            break;
          case 'Connexion':
            iconName = faKey;
            break;
          case 'Panier':
            iconName = faShoppingBasket;
            break;
          default:
            iconName = faHome;
        }
        return <CheckIconName name={route.name} icon={iconName} size={25} color={color} />;
      },
    })}
    tabBarOptions={{
      activeTintColor: '#3A2F2F',
      inactiveTintColor: 'gray',
      activeBackgroundColor: '#f1f2f6',
      showLabel: false,
    }}
  >
    <Tab.Screen name='Accueil' component={HomeStackScreen} />
    <Tab.Screen name='Espaces de travail' component={CoworkingStackScreen} />
    <Tab.Screen name='Profil' component={ProfileStackScreen} />
    <Tab.Screen name='Panier' component={BasketStackScreen} />
  </Tab.Navigator>
);

export default TabNavigator;
