import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

/**
 * Composant qui affiche le récapitulatif de la commande sous forme d'un tocket de caisse avec tous les détails
 */
const SummaryOrder = ({ articles = [], reservations = [], price = 0, points = 0 }) => (
  <View style={styles.orderContainer}>
    {/* RESERVATIONS */}
    {showReservations(reservations)}

    {/* ARTICLES */}
    {showArticles(articles)}

    {/* POINTS FIDELITE */}
    <View style={{ flexDirection: 'row', marginTop: '2.5%' }}>
      <Text style={{ fontSize: 17 }}>Points de fidélité :</Text>
      <Text style={{ flex: 1, fontSize: 17, textAlign: 'right' }}>{points}</Text>
    </View>

    {/* PRIX TOTAL DE LA COMMANDE */}
    <View style={{ flexDirection: 'row', marginTop: '5%' }}>
      <Text style={{ fontSize: 17, textAlign: 'right', fontWeight: 'bold' }}>Montant total TTC :</Text>
      <Text style={{ flex: 1, fontSize: 17, fontWeight: 'bold', textAlign: 'right' }}>{`${price}€`}</Text>
    </View>
    <View style={{ flexDirection: 'row' }}>
      <Text style={{ fontSize: 15, textAlign: 'right' }}>TVA 20% :</Text>
      <Text style={{ flex: 1, fontSize: 15, textAlign: 'right' }}>{`${(price * 0.2).toFixed(2)}€`}</Text>
    </View>
  </View>
);

/**
 * Affichage des réservations s'il y'en a
 * @param {Array} reservations
 */
function showReservations(reservations) {
  return reservations.length > 0 ? (
    <>
      <Text style={styles.title}>Réservations</Text>
      {reservations.map((reservation, index) => (
        <View key={index.toString()} style={styles.articleContainer}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>
              {`(${reservation.reservation_id.end - reservation.reservation_id.start}h) ${reservation.reservation_id.location_id.name}`}
            </Text>
            <Text style={{ position: 'absolute', right: 0, fontWeight: 'bold' }}>
              {`${reservation.reservation_id.location_id.location_category_id.price}€ /h`}
            </Text>
          </View>

          <View style={{ flex: 1, marginTop: '2.5%' }}>
            <Text>
              {`Début : ${new Date(
                reservation.reservation_id.year,
                reservation.reservation_id.month,
                reservation.reservation_id.day,
              ).toLocaleDateString()} à ${reservation.reservation_id.start}h`}
            </Text>
            <Text>
              {`Fin : ${new Date(
                reservation.reservation_id.year,
                reservation.reservation_id.month,
                reservation.reservation_id.day,
              ).toLocaleDateString()} à ${reservation.reservation_id.end}h`}
            </Text>
          </View>

          {/* PRIX DE LA RESERVATION */}
          <View style={styles.subtotalContainer}>
            <Text style={{ position: 'absolute', right: 0, fontStyle: 'italic' }}>{`Sous-total : ${reservation.reservation_id.price}€`}</Text>
          </View>
        </View>
      ))}

      <View style={styles.separator} />
    </>
  ) : null;
}

/**
 * Affichage des articles s'il y'en a
 * @param {Array} articles
 */
function showArticles(articles) {
  return articles.length > 0 ? (
    <>
      <Text style={styles.title}>Articles</Text>
      {articles.map((article, index) => (
        /* PRODUITS */
        <View key={index.toString()} style={styles.articleContainer}>
          <View style={{ flexDirection: 'row' }}>
            <Text style={{ fontWeight: 'bold' }}>{`(${article.quantity}) ${article.product_id.name}`}</Text>
            <Text style={{ position: 'absolute', right: 0, fontWeight: 'bold' }}>{`${article.product_id.price * article.quantity}€`}</Text>
          </View>

          {/* OPTIONS */}
          <View style={{ width: '100%' }}>
            {article.option_ids
              ? article.option_ids.map((option, key) => (
                  <View key={key.toString()} style={{ flexDirection: 'row' }}>
                    <Text style={{ marginLeft: '2.5%' }}>{`+ ${option.name}`}</Text>

                    <Text style={{ position: 'absolute', right: 0 }}>{`${(option.price || 0) * article.quantity}€`}</Text>
                  </View>
                ))
              : null}
          </View>

          {/* PRIX D'UN ARTICLE (produit + options) */}
          <View style={styles.subtotalContainer}>
            <Text style={{ position: 'absolute', right: 0, fontStyle: 'italic' }}>
              {article.price === 0 ? 'PROMO : 0€' : `Sous-total : ${article.price}€`}
            </Text>
          </View>
        </View>
      ))}

      <View style={styles.separator} />
    </>
  ) : null;
}

const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    backgroundColor: 'white',
    padding: 10,
  },
  orderContainer: {
    marginTop: 25,
    alignSelf: 'center',
    borderWidth: 1,
    borderColor: 'silver',
    borderRadius: 5,
    padding: 25,
    width: '100%',
  },
  articleContainer: {
    marginBottom: 20,
  },
  subtotalContainer: {
    width: '100%',
    marginTop: '2.5%',
    marginBottom: '7.5%',
  },
  title: {
    fontSize: 18,
    marginBottom: '5%',
    textDecorationLine: 'underline',
  },
  separator: {
    flex: 1,
    flexDirection: 'row',
    height: 0.5,
    backgroundColor: 'silver',
    marginBottom: '5%',
    marginTop: '5%',
    borderRadius: 5,
  },
});

export default SummaryOrder;
