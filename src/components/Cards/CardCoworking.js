import React from 'react';
import { StyleSheet, TouchableOpacity } from 'react-native';
import { Card } from 'react-native-elements';
import ShowImage from '../../utils/ShowImage';

/**
 * Composant qui affiche les espaces de travail
 */
const CardCoworking = ({ navigation, place }) => (
  <TouchableOpacity style={{ margin: 10, borderRadius: 5 }} onPress={() => navigation.navigate('Emplacement', { _id: place._id })}>
    <Card
      title={place.name}
      wrapperStyle={{ flex: 1, justifyContent: 'space-between' }}
      image={ShowImage(place.location_category_id.name)}
      containerStyle={styles.cardContainer}
    />
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  cardContainer: {
    borderRadius: 5,
    padding: 0,
    margin: 0,
    borderWidth: 0,
    width: 160,
    height: 250,
  },
  price: {
    fontSize: 20,
    fontWeight: 'bold',
    textAlign: 'center',
  },
});

export default CardCoworking;
