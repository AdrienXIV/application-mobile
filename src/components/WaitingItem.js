import React from 'react';
import { Text } from 'react-native';
import { Icon } from 'react-native-elements';
import * as Animatable from 'react-native-animatable';
import { Waiting } from './Styles/Theme';

/**
 * Composant qui affiche une page de chargement avec une animation rotative d'un logo
 */
const WaitingItem = ({ message, logoName = 'info-circle' }) => (
  <Waiting>
    <Text style={{ fontSize: 30, color: 'white', fontWeight: 'bold', margin: 10, textAlign: 'center' }}>{message}</Text>
    <Animatable.View animation='rotate' iterationCount='infinite' easing='linear'>
      <Icon name={logoName} type='font-awesome-5' color='white' size={40} />
    </Animatable.View>
  </Waiting>
);

export default WaitingItem;
